# Diagnostics Module
 The "Diagnostics" module is a set of functionality to provide application diagnostics and debugging support. The
 primary component in this module is the [$log service](http://transcend.bitbucket.org/#/api/transcend.diagnostics.$log)
 which extends AngularJS's [$log service](http://docs.angularjs.org/api/ng/service/$log) to provide a hierarchical
 tracking of operations with elapsed time calculations.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-diagnostics.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-diagnostics/get/master.zip](http://code.tsstools.com/bower-diagnostics/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-diagnostics](http://code.tsstools.com/bower-diagnostics)
* If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/diagnostics/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.diagnostics']);
```
## To Do
- Refactor the "treeLog" code:
    - Move the tree/node code into its own directive "tree"
    - Modify the treeLog directive to use the new "tree" directive
    - Give the treeLog an isolated scope and set defaults on the scope to use $log.logs by default