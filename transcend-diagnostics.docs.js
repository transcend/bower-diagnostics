/**
 * @ngdoc overview
 * @name transcend.diagnostics
 * @description
 # Diagnostics Module
 The "Diagnostics" module is a set of functionality to provide application diagnostics and debugging support. The
 primary component in this module is the {@link transcend.diagnostics.$log $log service}
 which extends AngularJS's [$log service](http://docs.angularjs.org/api/ng/service/$log) to provide a hierarchical
 tracking of operations with elapsed time calculations.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-diagnostics.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-diagnostics/get/master.zip](http://code.tsstools.com/bower-diagnostics/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-diagnostics](http://code.tsstools.com/bower-diagnostics)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/diagnostics/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.diagnostics']);
 ```
 ## To Do
 - Refactor the "treeLog" code:
 - Move the tree/node code into its own directive "tree"
 - Modify the treeLog directive to use the new "tree" directive
 - Give the treeLog an isolated scope and set defaults on the scope to use $log.logs by default
*/
/**
 * @ngdoc service
 * @name transcend.diagnostics.$log
 *
 * @description
 * Extends the default AngularJS [$log service](http://docs.angularjs.org/api/ng/service/$log) to provide a hierarchical
 * tracking of operations with elapsed time calculations.
 *
 <pre>
 $log.log('log message');
 $log.debug('debug message');
 $log.info('info message');
 $log.warn('warn message');
 $log.error('error message');

 var defer = $q.defer();
 $log.start('Doing something...', defer.promise);

 defer.resolve();
 // The log will automatically be marked as complete
 // and will calculate the time based on the promise.
 </pre>
 */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#linear
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Turns the nested log object into an array.
     *
     * @returns {Array} An array representation of the nested logs.
     */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#clear
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Clears the log cache.
     *
     * @returns {$log} The $log service.
     */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#start
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Starts a running log - an asynchronous log. Any other logs that happen during this process will logged as a
     * child under the running log (parent).
     *
     * @returns {$log} The $log service.
     */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#stop
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Stops a running log (an asynchronous log) so that it will no catch logs as it's children.
     *
     * @returns {$log} The $log service.
     */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#error
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Default {@link http://docs.angularjs.org/api/ng/service/$log AngularJS $log} error method.
     *
     * @returns {$log} The $log service.
     */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#info
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Default {@link http://docs.angularjs.org/api/ng/service/$log AngularJS $log} info method.
     *
     * @returns {$log} The $log service.
     */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#log
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Default {@link http://docs.angularjs.org/api/ng/service/$log AngularJS $log} log method.
     *
     * @returns {$log} The $log service.
     */
/**
     * @ngdoc method
     * @name transcend.diagnostics.$log#warn
     * @methodOf transcend.diagnostics.$log
     *
     * @description
     * Default {@link http://docs.angularjs.org/api/ng/service/$log AngularJS $log} warn method.
     *
     * @returns {$log} The $log service.
     */
/**
 * @ngdoc directive
 * @name transcend.diagnostics.directive:logTree
 *
 * @description
 * The 'logTree' directive provides a simple interactive tree structure to view your nested logs.
 *
 * @restrict A
 * @element ANY
 *
 * @requires $compile
 *
 * @param {object} treeModel Nested log object (natively built by the $log service).
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <input type="text" ng-model="logMsg" />
 <select ng-model="logType">
 <option value="debug">Debug</option>
 <option value="info">Info</option>
 <option value="warn">Warn</option>
 <option value="error">Error</option>
 </select>
 <button class="btn btn-primary" ng-click="log[logType](logMsg)">Log It!</button>
 <div log-tree tree-model="log.logs" filter="search"></div>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.diagnostics'])
 .controller('Ctrl', function($scope, $log) {
    // Create some initial logs.
    var start1 = $log.start('Loading application').last,
      start2 = $log.start('Rendered 5 views').last;

    $log.info('Rendered view 1');
    $log.error('Failed to render view 2');
    $log.debug('Rendered view 3')
    $log.warn('Almost rendered view 4')
    $log.log('Rendered view 5')

    $log.stop(start2);
    $log.stop(start1);

    // Set defaults.
    $scope.logMsg = 'Test Log Message'
    $scope.logType = 'error';

    // Expose the log object.
    $scope.log = $log;
});
 </file>
 </example>
 */
/**
 * @ngdoc filter
 * @name transcend.diagnostics.$time
 *
 * @description
 * Provides an elapsed filter that:
 * - takes a start value and returns the equivalent time in milliseconds if the value is < 1000 and in seconds otherwise
 * - takes start and finish values and returns the difference between them in milliseconds if the start value is < 1000
 *   and seconds if the start value is > 1000.
 *
 <h3>Using the Elapsed Filter</h3>
 <pre>
 var elapsedFilter = $filter('elapsed');
 elapsedFilter(1000, 1300);
 // returns:  '300'

 elapsedFilter(200);
 // returns:  '200 ms'
 </pre>
 */
/**
         * @ngdoc method
         * @name transcend.diagnostics.$time#format
         * @propertyOf transcend.diagnostics.$time
         *
         * @description
         * converts milliseconds into other metric values of time.
         *
         * @param {String} milliseconds The time in milliseconds.
         * @param {object} suffixMap A mapping of time units to alias. Example: ms: millis, sec: seconds, etc.
         *
         * @returns {*} the formatted time
         */
/**
 * @ngdoc filter
 * @name transcend.diagnostics.elapsed
 *
 * @description
 * Provides the ability to show elapsed time in human readable text. If a second parameter "end date" is not passed in,
 * than it will supplement the end date with the current date time.
 *
 * @require transcend.diagnostics.$time
 *
 * @param {date|string} start The start date.
 * @param {date|string} end The end date.
 * @param {object} suffixMap A mapping of time units to alias. Example: ms: millis, sec: seconds, etc.
 *
 <pre>
 var elapsedFilter = $filter('elapsed');
 expect(elapsedFilter(1000, 1200)).toEqual('200 ms');
 expect(elapsedFilter(1000, 2000)).toEqual('1 sec');
 expect(elapsedFilter(1000, 61000)).toEqual('1 min');
 expect(elapsedFilter(1000, 3601000)).toEqual('1 hr');
 expect(elapsedFilter(1000, 86401000)).toEqual('1 day');
 </pre>
 */
/**
 * @ngdoc filter
 * @name transcend.diagnostics.time
 *
 * @description
 * Provides a filter to show milliseconds in human readable text.
 *
 * @requires $time
 <pre>
 var timeFilter = $filter('elapsed');
 expect(timeFilter(100)).toEqual('100 ms');
 expect(timeFilter(1000)).toEqual('1 sec');
 </pre>
 */
